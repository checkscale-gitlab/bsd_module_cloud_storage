# https://stackoverflow.com/questions/64989080/terraform-modules-output-from-for-each
output "common_bucket_names" {
  description = "The names of the made common buckets"
  value = [
    for bucket in google_storage_bucket.common_buckets : bucket.name
  ]
}

output "extra_bucket_names" {
  description = "The names of the made extra buckets"
  value = [
    for bucket in google_storage_bucket.extra_buckets : bucket.name
  ]
}
