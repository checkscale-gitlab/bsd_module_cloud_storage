/**
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


data "google_service_account" "kitchen_account" {
  account_id = "terraform-kitchen"
  project    = var.project_id
}


# resource "time_rotating" "mykey_rotation" {
#   rotation_hours = 1
# }


resource "google_service_account_key" "int_test" {
  service_account_id = data.google_service_account.kitchen_account.email
  # keepers = {
  #   rotation_time = time_rotating.mykey_rotation.rotation_rfc3339
  # }
}
